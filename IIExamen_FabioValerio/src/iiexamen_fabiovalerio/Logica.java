/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iiexamen_fabiovalerio;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Fabio_2
 */
public class Logica extends JPanel implements KeyListener {

    Numeros n = new Numeros();
    int Horas = 0;
    int Minutos = 0;
    int Segundos = 0;
    boolean a = true;

    public Logica() {
        setFocusable(true);
        addKeyListener(this);
    }

    @Override
    public void paint(Graphics g) {
//        n.pintaruno(g, n.uno,75,20);
//        n.pintardos(g, n.uno, 150, 20);
//        n.pintarTres(g,n.tres,230,20);
//        n.pintarCuatro(g, n.cuatro, 310, 20);
//        n.pintarcinco(g, n.cinco, 390, 20);
////        n.pintarSeis(g, 75, 150);
//        n.pintarSiete(g, 150, 150);
//        n.pintarOcho(g, 230, 150);
//        n.pintarNueve(g, 310, 150);
//        n.pintarCero(g, 390,150);
        cronometro(g);
        if (Horas == 1) {
            n.pintarCero(g, 75, 20);
            n.pintaruno(g, n.uno, 150, 20);
        } else if (Horas == 2) {
            n.pintarCero(g, 75, 20);
            n.pintardos(g, n.uno, 150, 20);
        } else if (Horas == 3) {
            n.pintarCero(g, 75, 20);
            n.pintarTres(g, n.uno, 150, 20);
        } else if (Horas == 4) {
            n.pintarCero(g, 75, 20);
            n.pintarCuatro(g, n.uno, 150, 20);
        } else if (Horas == 5) {
            n.pintarCero(g, 75, 20);
            n.pintarcinco(g, n.uno, 150, 20);
        } else if (Horas == 6) {
            n.pintarCero(g, 75, 20);
            n.pintarSeis(g, 150, 20);
        } else if (Horas == 7) {
            n.pintarCero(g, 75, 20);
            n.pintarSiete(g, 150, 20);
        } else if (Horas == 8) {
            n.pintarCero(g, 75, 20);
            n.pintarOcho(g, 150, 20);
        } else if (Horas == 9) {
            n.pintarCero(g, 75, 20);
            n.pintarNueve(g, 150, 20);
        } else if (Horas == 0) {
            n.pintarCero(g, 75, 20);
            n.pintarCero(g, 150, 20);
        }
        g.fillRect(250, 40, 20, 20);
        g.fillRect(250, 80, 20, 20);

        if (Minutos == 0) {
            n.pintarCero(g, 310, 20);

        } else if (Minutos == 1) {
            n.pintaruno(g, n.uno, 310, 20);
        } else if (Minutos == 2) {
            n.pintardos(g, n.uno, 310, 20);
        } else if (Minutos == 3) {
            n.pintarTres(g, n.uno, 310, 20);
        } else if (Minutos == 4) {
            n.pintarCuatro(g, n.uno, 310, 20);
        } else if (Minutos == 5) {
            n.pintarcinco(g, n.uno, 310, 20);
        } else if (Minutos == 6) {
            Minutos = 0;
            Horas += 1;
        }

        if (a) {
            Segundos++;
        }
        if (Segundos > 9) {
            Segundos = 0;
            Minutos++;
        }
    }

    public void cronometro(Graphics g) {
        switch (Segundos) {
            case 0:
//                n.pintarCero(g, 75, 20);
                n.pintarCero(g, 390, 20);
                break;
            case 1:
//                n.pintarCero(g, 75, 20);
                n.pintaruno(g, n.uno, 390, 20);
                break;
            case 2:
//                n.pintarCero(g, 75, 20);
                n.pintardos(g, n.uno, 390, 20);
                break;
            case 3:
//                n.pintarCero(g, 75, 20);
                n.pintarTres(g, n.uno, 390, 20);
                break;
            case 4:
//                n.pintarCero(g, 75, 20);
                n.pintarCuatro(g, n.uno, 390, 20);
                break;
            case 5:
//                n.pintarCero(g, 75, 20);
                n.pintarcinco(g, n.uno, 390, 20);
                break;
            case 6:
//                n.pintarCero(g, 75, 20);
                n.pintarSeis(g, 390, 20);
                break;
            case 7:
//                n.pintarCero(g, 75, 20);
                n.pintarSiete(g, 390, 20);
                break;
            case 8:
//                n.pintarCero(g, 75, 20);
                n.pintarOcho(g, 390, 20);
                break;
            case 9:
//                n.pintarCero(g, 75, 20);
                n.pintarNueve(g, 390, 20);
                break;
            default:
                n.pintarCero(g, 390, 20);
                break;
        }
    }

    @Override
    public void keyTyped(KeyEvent ke
    ) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        switch (ke.getKeyCode()) {
            case KeyEvent.VK_R:
                Segundos = 0;
                Horas = 0;
                Minutos = 0;
                break;
            case KeyEvent.VK_D:
                Segundos = 0;
                Horas = 0;
                Minutos = 0;
                a = false;
                break;
            case KeyEvent.VK_P:
                a = false;
                break;
            case KeyEvent.VK_I:
                a = true;
                break;
            default:
                break;
        }
        repaint();
    }

    @Override
    public void keyReleased(KeyEvent ke
    ) {
    }

}
