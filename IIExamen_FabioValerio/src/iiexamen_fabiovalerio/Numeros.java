/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iiexamen_fabiovalerio;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Fabio_2
 */
public class Numeros {

    private int CoorX;
    private int CoorY;

    public void setCoorX(int CoorX) {
        this.CoorX = CoorX;
    }

    public void setCoorY(int CoorY) {
        this.CoorY = CoorY;
    }

    public int getCoorX() {
        return CoorX;
    }

    public int getCoorY() {
        return CoorY;
    }

    public Numeros() {
    }

    int[][] uno = {
        {0, 0, 1},
        {0, 0, 1},
        {0, 0, 1},
        {0, 0, 1},
        {0, 0, 1}
    };
    int[][] dos = {
        {1, 1, 1},
        {0, 0, 1},
        {1, 1, 1},
        {1, 0, 0},
        {1, 1, 1}};

    int[][] tres = {
        {1, 1, 1},
        {0, 0, 1},
        {1, 1, 1},
        {0, 0, 1},
        {1, 1, 1}};

    int[][] cuatro = {
        {1, 0, 1},
        {1, 0, 1},
        {1, 1, 1},
        {0, 0, 1},
        {0, 0, 1}};

    int[][] cinco = {
        {1, 1, 1},
        {1, 0, 0},
        {1, 1, 1},
        {0, 0, 1},
        {1, 1, 1}};

    int[][] seis = {
        {1, 1, 1},
        {1, 0, 0},
        {1, 1, 1},
        {1, 0, 1},
        {1, 1, 1}};

    int[][] siete = {
        {1, 1, 1},
        {0, 0, 0},
        {0, 0, 1},
        {0, 0, 1},
        {0, 0, 1}};

    int[][] ocho = {
        {1, 1, 1},
        {1, 0, 1},
        {1, 1, 1},
        {1, 0, 1},
        {1, 1, 1}};

    int[][] nueve = {
        {1, 1, 1},
        {1, 0, 1},
        {1, 1, 1},
        {0, 0, 1},
        {1, 1, 1}};

    int[][] cero = {
        {1, 1, 1},
        {1, 0, 1},
        {1, 0, 1},
        {1, 0, 1},
        {1, 1, 1}};

    public void pintaruno(Graphics g, int[][] numero, int CoorX, int CoorY) {
        g.setColor(Color.BLACK);
        g.fillRect(CoorX + 40, CoorY, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 20, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 40, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 60, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 60, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 80, 20, 20);

    }

    public void pintardos(Graphics g, int[][] numero, int CoorX, int CoorY) {
        g.setColor(Color.BLACK);
        g.fillRect(CoorX, CoorY, 20, 20);
        g.fillRect(CoorX + 20, CoorY, 20, 20);
        g.fillRect(CoorX + 40, CoorY, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 20, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 40, 20, 20);
        g.fillRect(CoorX + 20, CoorY + 40, 20, 20);
        g.fillRect(CoorX, CoorY + 40, 20, 20);
        g.fillRect(CoorX, CoorY + 60, 20, 20);
        g.fillRect(CoorX, CoorY + 80, 20, 20);
        g.fillRect(CoorX + 20, CoorY + 80, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 80, 20, 20);

    }

    public void pintarTres(Graphics g, int[][] numero, int CoorX, int CoorY) {
        g.setColor(Color.BLACK);
        g.fillRect(CoorX, CoorY, 20, 20);
        g.fillRect(CoorX + 20, CoorY, 20, 20);
        g.fillRect(CoorX + 40, CoorY, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 20, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 40, 20, 20);
        g.fillRect(CoorX + 20, CoorY + 40, 20, 20);
        g.fillRect(CoorX, CoorY + 40, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 60, 20, 20);
        g.fillRect(CoorX, CoorY + 80, 20, 20);
        g.fillRect(CoorX + 20, CoorY + 80, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 80, 20, 20);

    }

    public void pintarCuatro(Graphics g, int[][] numero, int CoorX, int CoorY) {
        g.setColor(Color.BLACK);
        g.fillRect(CoorX, CoorY, 20, 20);
        g.fillRect(CoorX, CoorY + 20, 20, 20);
        g.fillRect(CoorX + 40, CoorY, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 20, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 40, 20, 20);
        g.fillRect(CoorX + 20, CoorY + 40, 20, 20);
        g.fillRect(CoorX, CoorY + 40, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 60, 20, 20);
//        g.fillRect(CoorX, CoorY + 80, 20, 20);
//        g.fillRect(CoorX + 20, CoorY + 80, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 80, 20, 20);
    }

    public void pintarcinco(Graphics g, int[][] numero, int CoorX, int CoorY) {
        g.setColor(Color.BLACK);
        g.fillRect(CoorX, CoorY, 20, 20);
        g.fillRect(CoorX + 20, CoorY, 20, 20);
        g.fillRect(CoorX + 40, CoorY, 20, 20);
        g.fillRect(CoorX, CoorY + 20, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 40, 20, 20);
        g.fillRect(CoorX + 20, CoorY + 40, 20, 20);
        g.fillRect(CoorX, CoorY + 40, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 60, 20, 20);
        g.fillRect(CoorX, CoorY + 80, 20, 20);
        g.fillRect(CoorX + 20, CoorY + 80, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 80, 20, 20);
    }

    public void pintarSeis(Graphics g, int CoorX, int CoorY) {
        g.setColor(Color.BLACK);
        g.fillRect(CoorX, CoorY, 20, 20);
        g.fillRect(CoorX + 20, CoorY, 20, 20);
        g.fillRect(CoorX + 40, CoorY, 20, 20);
        g.fillRect(CoorX, CoorY + 20, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 40, 20, 20);
        g.fillRect(CoorX + 20, CoorY + 40, 20, 20);
        g.fillRect(CoorX, CoorY + 40, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 60, 20, 20);
        g.fillRect(CoorX, CoorY + 80, 20, 20);
        g.fillRect(CoorX, CoorY + 60, 20, 20);
        g.fillRect(CoorX + 20, CoorY + 80, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 80, 20, 20);
    }

    public void pintarSiete(Graphics g, int CoorX, int CoorY) {
        g.setColor(Color.BLACK);
        g.fillRect(CoorX, CoorY, 20, 20);
        g.fillRect(CoorX + 20, CoorY, 20, 20);
        g.fillRect(CoorX + 40, CoorY, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 20, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 40, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 60, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 60, 20, 20);
        g.fillRect(CoorX + 40, CoorY + 80, 20, 20);

    }

    public void pintarOcho(Graphics g, int CoorX, int CoorY) {
        g.setColor(Color.BLACK);
        g.fillRect(CoorX, CoorY, 20, 20);
        g.fillRect(CoorX + 20, CoorY, 20, 20);
        g.fillRect(CoorX + 40, CoorY, 20, 20);
        g.fillRect(CoorX, CoorY+20, 20, 20);
        g.fillRect(CoorX, CoorY+40, 20, 20);
        g.fillRect(CoorX+20, CoorY+40, 20, 20);
        g.fillRect(CoorX+40, CoorY+40, 20, 20);
        g.fillRect(CoorX+40, CoorY+20, 20, 20);
        g.fillRect(CoorX, CoorY+60, 20, 20);
        g.fillRect(CoorX, CoorY+80, 20, 20);
        g.fillRect(CoorX, CoorY+80, 60, 20);
        g.fillRect(CoorX+40, CoorY+60, 20, 20);

    }
    public void pintarNueve(Graphics g, int CoorX, int CoorY) {
        g.setColor(Color.BLACK);
        g.fillRect(CoorX, CoorY, 60, 20);
        g.fillRect(CoorX, CoorY, 20, 60);
        g.fillRect(CoorX, CoorY+40, 60, 20);
        g.fillRect(CoorX+40, CoorY, 20, 100);
        g.fillRect(CoorX, CoorY+80, 60, 20);
    }
    public void pintarCero(Graphics g, int CoorX, int CoorY){
        g.setColor(Color.BLACK);
        g.fillRect(CoorX, CoorY, 60, 20);
        g.fillRect(CoorX, CoorY, 20, 100);
        g.fillRect(CoorX, CoorY+80, 60, 20);
        g.fillRect(CoorX+40, CoorY, 20, 100);
    }
    
    
    
}
